class AddModifiedAtAndModifiedCountToComments < ActiveRecord::Migration[5.2]
  def change
    add_column :comments, :modified_at, :datetime
    add_column :comments, :modified_count, :integer, default: 0
  end
end
