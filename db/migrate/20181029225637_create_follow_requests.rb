class CreateFollowRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :follow_requests do |t|
      t.references :account, foreign_key: true
      t.references :target_account, foreign_key: { to_table: :accounts }
      t.string :uri

      t.timestamps
    end
  end
end
