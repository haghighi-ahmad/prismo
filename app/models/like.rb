# frozen_string_literal: true

class Like < ApplicationRecord
  belongs_to :activitypub_object, foreign_key: :likeable_id

  # @todo get rid of that polymorphic association
  belongs_to :likeable, polymorphic: true, counter_cache: true

  belongs_to :account
end
