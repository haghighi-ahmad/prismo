# frozen_string_literal: true

class Comment < ApplicationRecord
  include HasUUIDConcern
  include ActivityPub::ObjectConcern
  include PgSearch

  has_closure_tree

  HOT_DAYS_LIMIT = 40

  pg_search_scope :search, against: :body,
                           using: { tsearch: { prefix: true } }

  belongs_to :story, counter_cache: true
  belongs_to :account, counter_cache: true, optional: true
  has_many :likes, as: :likeable
  has_many :flags, as: :flaggable

  def cache_depth
    update_attributes(depth_cached: depth)
  end

  def cache_body
    update_attributes(
      body_html: BodyParser.new(body).call
    )
  end

  def object_type
    :note
  end
end
