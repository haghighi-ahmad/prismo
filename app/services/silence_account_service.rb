# frozen_string_literal: true

class SilenceAccountService
  def call(account)
    account.update(silenced: true)
  end
end
