# frozen_string_literal: true

class XNodeinfo2Serializer < Granola::Serializer
  include RoutingHelper

  def initialize
    super(nil)
  end

  def data
    {
      version: '1.0',
      server: server,
      protocols: ['activitypub'],
      openRegistrations: Setting.open_registrations,
      usage: usage,
      services: services
    }
  end

  def server
    {
      baseUrl: root_url,
      name: Setting.site_title,
      software: 'prismo',
      version: Prismo::Version
    }
  end

  def usage
    {
      users: {
        total: User.all.count,
        activeHalfyear: Account.local.active_halfyear.count,
        activeMonth: Account.local.active_month.count
      },

      localPosts: ActivityPubPost.local.count,
      localComments: ActivityPubComment.local.count
    }
  end

  def services
    {
      inbound: [],
      outbound: []
    }
  end
end
