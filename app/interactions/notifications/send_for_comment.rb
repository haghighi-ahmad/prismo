# frozen_string_literal: true

class Notifications::SendForComment < ActiveInteraction::Base
  object :comment, class: ActivityPubComment

  def execute
    return if comment.parent.present? && comment.parent.account == comment.account

    notifable = comment
    context = comment.root

    if comment.parent.is_a?(ActivityPubComment)
      event_key = 'comment_reply'
      recipient = comment.parent.account
    else
      event_key = 'story_reply'
      recipient = comment.root.account
    end

    CreateNotificationJob.call(
      event_key,
      author: comment.account,
      recipient: recipient,
      notifable: notifable,
      context: context
    )
  end
end
