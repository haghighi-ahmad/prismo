import BaseController from './base_controller'
import moment from 'moment'

export default class extends BaseController {
  connect () {
    this.update()
    this.interval = setInterval(this.update.bind(this), 60000)
  }

  disconnect () {
    clearInterval(this.interval)
  }

  update () {
    this.element.innerHTML = this.date.fromNow()
  }

  get date () {
    return moment(new Date(this.element.getAttribute('data-datetime')))
  }
}
