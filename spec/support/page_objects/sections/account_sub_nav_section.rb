# frozen_string_literal: true

class AccountSubNavSection < SitePrism::Section
  element :comments_link, 'a', text: 'Comments'
end
