require 'rails_helper'

describe ActivityPub::AcceptFollowSerializer do
  let(:follow_request) { create(:follow_request) }

  describe '#as_json' do
    let(:result) { described_class.new(follow_request).as_json }

    it { expect { result }.to_not raise_exception }
  end
end
